package com.example.maxim.chrono;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class EcranDemarageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecran_demarage);

    }
    public void go(View view) {
        Intent passerversmenu = new Intent();
        passerversmenu.setClass(this,MenuActivity.class); // MenuActivity est l'activité affichant le menu elle se lance à l'appui du bouton Go
        startActivity(passerversmenu);
        finish();
    }
}


