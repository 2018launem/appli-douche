package com.example.maxim.chrono;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

import java.util.Calendar;
import java.util.Date;

public class ChronoActivity extends AppCompatActivity {

    Chronometer chrono;
    String timer;
    Date jour;
    MediaPlayer musique;
    int duree;
    String minutes;
    String secondes;
    private int Voix;
    private Button stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chrono);
        Intent depuisMenuActivity = getIntent();


        chrono = (Chronometer) findViewById(R.id.simpleChronometer);
        chrono.setBase(SystemClock.elapsedRealtime());                        //creation du chrono
        chrono.start();
        Voix=depuisMenuActivity.getIntExtra("voix",1);       //silence par defaut


        if (Voix==2) {
            musique = MediaPlayer.create(this,R.raw.appli_douche_voix1);     //musique prends la valeur de la musique selectionnée
            musique.start();

        }
        if (Voix==3) {
            musique = MediaPlayer.create(this,R.raw.musique_film);
            musique.start();

        }
    }


    public void methodeStop(View v) {
        chrono.stop();
        timer = chrono.getText().toString();
        minutes = timer.substring(0,2);
        secondes = timer.substring(3,5);
        int min = Integer.valueOf(minutes);         //arret du chrono mesure du temps
        int sec = Integer.valueOf(secondes);
        duree = min*60 + sec;
        if (Voix==2) {
            musique.stop();                        //stop la musique s'il y en avait une
        }
        if (Voix==3) {
            musique.stop();
        }



        SharedPreferences sp=   getSharedPreferences("statsEau", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =sp.edit();
        editor.putFloat("dureeJ0",sp.getFloat("dureeJ1",0));
        editor.putFloat("dureeJ1",sp.getFloat("dureeJ2",0));
        editor.putFloat("dureeJ2",sp.getFloat("dureeJ3",0));
        editor.putFloat("dureeJ3",sp.getFloat("dureeJ4",0));
        editor.putFloat("dureeJ4",sp.getFloat("dureeJ5",0));       //decalage des valeurs du graphe en barres la derniere valeur prends la valeur de l'avant derniere
        editor.putFloat("dureeJ5",sp.getFloat("dureeJ6",0));
        editor.putFloat("dureeJ6",sp.getFloat("dureeJ7",0));
        editor.putFloat("dureeJ7",sp.getFloat("dureeJ8",0));
        editor.putFloat("dureeJ8",sp.getFloat("dureeJ9",0));
        editor.putFloat("dureeJ9",(float)duree);
        editor.apply();
        Intent passerversResultats = new Intent();
        passerversResultats.setClass(this, Resultats_Activity.class);
        startActivity(passerversResultats);
        }
    public void versMenu(View view) {
        if (Voix==2) {
            musique.stop();                         //bouton retour u menu on stoppe la musique
        }
        if (Voix==3) {
            musique.stop();
        }
        Intent passerversmenu = new Intent();
        passerversmenu.setClass(this, MenuActivity.class); //envoie vers menu
        startActivity(passerversmenu);
        finish();
    }

}




