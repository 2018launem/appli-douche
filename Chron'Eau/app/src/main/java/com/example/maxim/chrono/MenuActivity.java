package com.example.maxim.chrono;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class MenuActivity extends AppCompatActivity {
    private String choixmenu;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Intent depuisResultatsActivity = getIntent();
        List<String> l = new ArrayList<String>();
        l.add("Douche silencieuse");                 //ajoute les valeurs au spinner
        l.add("Voix femme");
        l.add("Voix film");

        spinner = findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,l);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);








    }
    public void afficherstat(View view) {
        Intent passerversstat = new Intent();
        passerversstat.setClass(this,StatistiquesActivity.class); // ActivityStat est l'activité affichant les stats
        startActivity(passerversstat);                //bouton vers les stats
        finish();


    }
    public void afficherconseils(View view) {
        Intent passerversconseils = new Intent();
        passerversconseils.setClass(this, ConseilsActivity.class); //envoie vers conseils
        startActivity(passerversconseils);
        finish();
    }


    public void démarrer(View view) {                       //bouton play vers le  chrono
        choixmenu = spinner.getSelectedItem().toString();   //prends la valeur du spinner à l'appui sur play
        if (choixmenu.equals("Douche silencieuse")) {
            Intent passerversChrono = new Intent(this,ChronoActivity.class);  //envoie un numero correspondant au choix de musique via un intent
            passerversChrono.putExtra("voix",1);
            startActivity(passerversChrono);
            finish();

        }
        if (choixmenu.equals("Voix femme")) {
            Intent passerversChrono = new Intent(this, ChronoActivity.class);
            passerversChrono.putExtra("voix", 2);
            startActivity(passerversChrono);
            finish();

        }
        if (choixmenu.equals("Voix film")) {
            Intent passerversChrono = new Intent(this, ChronoActivity.class);
            passerversChrono.putExtra("voix", 3);
            startActivity(passerversChrono);
            finish();
        }




    }
}





