package com.example.maxim.chrono;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.util.Vector;

public class StatistiquesActivity extends AppCompatActivity {
    private Float dureeCourante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistiques);
        Intent depuisResultatsActivity = getIntent();

        SharedPreferences sp =   getSharedPreferences("statsEau", Context.MODE_PRIVATE);
        dureeCourante =sp.getFloat("dureeJ9",0); //on recupere le dernier temps avec le sharedpreference

        Vector<Float> dureeDouches = new Vector <Float>();

        dureeDouches.add( sp.getFloat("dureeJ0",0));
        dureeDouches.add( sp.getFloat("dureeJ1",0));
        dureeDouches.add( sp.getFloat("dureeJ2",0));
        dureeDouches.add( sp.getFloat("dureeJ3",0));
        dureeDouches.add( sp.getFloat("dureeJ4",0));             //on importe les derniers temps
        dureeDouches.add( sp.getFloat("dureeJ5",0));
        dureeDouches.add( sp.getFloat("dureeJ6",0));
        dureeDouches.add( sp.getFloat("dureeJ7",0));
        dureeDouches.add( sp.getFloat("dureeJ8",0));
        dureeDouches.add( sp.getFloat("dureeJ9",0));

        //creation  du graphique en barre

        GraphView graph = (GraphView) findViewById(R.id.graph);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[] {
                new DataPoint(1, dureeDouches.get(0)),
                new DataPoint(2, dureeDouches.get(1)),
                new DataPoint(3, dureeDouches.get(2)),
                new DataPoint(4, dureeDouches.get(3)),
                new DataPoint(5, dureeDouches.get(4)),
                new DataPoint(6, dureeDouches.get(5)),
                new DataPoint(7, dureeDouches.get(6)),
                new DataPoint(8, dureeDouches.get(7)),
                new DataPoint(9, dureeDouches.get(8)),
                new DataPoint(10, dureeDouches.get(9)),

        });
        graph.getGridLabelRenderer().setVerticalAxisTitle("Durée(s)");   //nom de l'axe des ordonnés
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(11);      //taille de la fenetre en x
        graph.getViewport().setScrollable(true);

        series.setSpacing(50);
        graph.addSeries(series);

        int compteurDouches=0;
        float total=0f;
        for (int i=0;i<dureeDouches.size();i++) {                   //on compte les douches stockées en mémoire différente de 0 seconde
            if (dureeDouches.get(i)!=0){
                compteurDouches+=1;
                total+=dureeDouches.get(i);
            }
        }
        int moyenne;
        if (compteurDouches!=0)
            moyenne =(int)(((int) total)/compteurDouches);        //calcul de la moyenne sur ce nombre de douche
        else
            moyenne=0;                                            //cas particulier de l'utilisation initiale
        int min=  moyenne/60;                                     //conversion secondes minutes
        int secondes = moyenne%60;

        TextView textStats = findViewById(R.id.textStats);           //textview affichant les données calculées plus haut
        textStats.setText("Tu passes en moyenne pour les "+compteurDouches+" dernieres douches "+Integer.toString(min)+" minutes et "+Integer.toString(secondes)+" secondes sous la douche !"+"\n"+"Cela correspond à " + Float.toString((float)moyenne*0.3f )+ " litres d'eau ! ");


    }
    public void versResultats(View view) {
        Intent versResultatsActivity = new Intent();
        versResultatsActivity.setClass(this, Resultats_Activity.class); //vers la classe ResultatsActivity à l'appui du bouton
        startActivity(versResultatsActivity);
        finish();
    }
    public void versMenu(View view) {
        Intent versMenuActivity = new Intent();
        versMenuActivity.setClass(this, MenuActivity.class); //vers la classe MenuActivity à l'appui du bouton
        startActivity(versMenuActivity);
        finish();
    }

    public void envoyerSMS(View view) {
        Intent versAppSMS = new Intent(Intent.ACTION_SENDTO);           //envoie d'un sms à un ami sur le dernier temps réalisé
        versAppSMS.setData(Uri.parse("smsto:"));
        String msgSMS = "Aujourd'hui ma douche a duré "+dureeCourante+" secondes. Télécharges Chron'Eau pour connaitre ton temps ! ";
        versAppSMS.putExtra("sms_body",msgSMS);
        startActivity(versAppSMS);
    }

}

