package com.example.maxim.chrono;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Random;
import java.util.Vector;

public class ConseilsActivity extends AppCompatActivity {
    private TextView TexteConseil;
    private Vector<String> listeConseil;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conseils);



        Intent depuisResultatsActivity = getIntent();

//on rentre les différents conseils

        listeConseil=new Vector<String>();
        //On affecte au vecteur les conseils
        listeConseil.add("Privilégie les transports en commun au lieu de la voiture");
        listeConseil.add("Pense à trier tes déchets");
        listeConseil.add("Fais du compost avec les déchets de ta cuisine");
        listeConseil.add("Coupe l'eau du robinet quand tu te brosses les dents");
        listeConseil.add("Achète des aliments issus de l'agriculture biologique et durable");
        listeConseil.add("Si tu as un jardin, cultive ton propre potager au lieu d'acheter tes produits en supermarché");
        listeConseil.add("Ne laisse pas tes appareils électroniques en veille quand tu ne les utilises pas");
        listeConseil.add("Veille à bien éteindre la lumière quand tu quittes une pièce");
        listeConseil.add("Utilise des écocups réutilisables au lieu de gobelets jetables à usage unique");
        listeConseil.add("Diminue ta vitesse en voiture: 120km/h au lieu de 130km/h réduit grandement les émissions de CO2 !");
        listeConseil.add("Arrête d'acheter de l'eau en bouteille: l'eau du robinet est filtrée et parfaitement potable");


        //On selectionne un des conseils à afficher au hasard
        Random r = new Random();
        int nbConseils = listeConseil.size();
        int nombreAleatoire = r.nextInt(nbConseils-1);

        TexteConseil = findViewById(R.id.textView1);
        TexteConseil.setText(listeConseil.get(nombreAleatoire)); //on l'affiche
    }

    public void versResultats(View view) {
        Intent versResultatsActivity = new Intent();
        versResultatsActivity.setClass(this, Resultats_Activity.class); //vers la classe ResultatsActivity à l'appui sur le bouton
        startActivity(versResultatsActivity);
        finish();
    }
    public void versMenu(View view) {
        Intent versMenuActivity = new Intent();
        versMenuActivity.setClass(this, MenuActivity.class); //vers la classe ResultatsActivity à l'appui sur le bouton
        startActivity(versMenuActivity);
        finish();
    }
}
