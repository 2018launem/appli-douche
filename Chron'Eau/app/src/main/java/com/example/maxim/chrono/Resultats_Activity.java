package com.example.maxim.chrono;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;


public class Resultats_Activity extends AppCompatActivity {
    //Création des différentes variables
    private Button boutonMenu;
    private Button boutonStatistiques;
    private Button boutonConseils;
    private TextView TextView1;
    private TextView TextView2;
    private Float tempsDoucheSecondes;
    private Float debit;
    private Float nombreDeLitres;
    private ImageView Image1;
    private ImageView Image2;
    private ImageView Image3;
    private ImageView Image4;
    private ImageView Image1bis;
    private ImageView Image2bis;
    private ImageView Image3bis;
    private ImageView Image4bis;
    private MediaPlayer musique1;
    private MediaPlayer musique2;
    private MediaPlayer musique3;
    private MediaPlayer musique4;

    public Resultats_Activity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultats_);

        Intent depuisChronoActivity = getIntent();

        //Recherche de la valeur du temps dans l'activité precedente
        SharedPreferences sp=   getSharedPreferences("statsEau", Context.MODE_PRIVATE);


        tempsDoucheSecondes =sp.getFloat("dureeJ9",0);



        // Le débit prend la valeur 0.3 L/s
        debit = 0.3f;


        // On calcule le nombre de litres consommés
        nombreDeLitres = tempsDoucheSecondes * debit;


        // affichage du premier texte qui indique les quantitées consomées et le temps passé sous la douche
        TextView1 = findViewById(R.id.textView1);
        TextView1.setText(" Aujourd'hui, ta douche a duré " + tempsDoucheSecondes + " secondes, ce qui correspond à " + nombreDeLitres + " litres consommés ! ");

        //affichage du second texte qui indique un message personalisé en fonction du temps passé sous la douche et d'un nombre d'images correspondant
        TextView2 = findViewById(R.id.textView2);
        Image1 = findViewById(R.id.imageView1);
        Image2 = findViewById(R.id.imageView2);
        Image3 = findViewById(R.id.imageView3);
        Image4 = findViewById(R.id.imageView4);
        Image1bis = findViewById(R.id.imageView1bis);
        Image2bis = findViewById(R.id.imageView2bis);
        Image3bis = findViewById(R.id.imageView3bis);
        Image4bis = findViewById(R.id.imageView4bis); //références des différents objets du layout
        Image1.setVisibility(View.INVISIBLE);
        Image2.setVisibility(View.INVISIBLE);
        Image3.setVisibility(View.INVISIBLE);
        Image4.setVisibility(View.INVISIBLE);
        Image1bis.setVisibility(View.VISIBLE);
        Image2bis.setVisibility(View.VISIBLE);
        Image3bis.setVisibility(View.VISIBLE);
        Image4bis.setVisibility(View.VISIBLE);

        //affichage en fonction du temps réalisé

        if (tempsDoucheSecondes < 60) {
            TextView2.setText(" C'est bien beau de vouloir économiser l'eau, mais il faudrait penser à se laver un minimum quand même ! ");
            //affiche le nombre d'images indiqué petit à petit avec un petit ding avec l'utilisation de delais
            musique1 = MediaPlayer.create(this,R.raw.cling2);
            musique2 = MediaPlayer.create(this,R.raw.cling2);
            musique3 = MediaPlayer.create(this,R.raw.cling3);
            musique4 = MediaPlayer.create(this,R.raw.cling4);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique1.start();
                            musique1.setVolume(0.2f,0.2f);
                            Image1.setVisibility(View.VISIBLE);
                            Image1bis.setVisibility(View.INVISIBLE);
                        }
                    }, 500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique1.stop();
                        }
                    }, 1000);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique2.start();
                            musique2.setVolume(0.2f,0.2f);
                            Image2.setVisibility(View.VISIBLE);
                            Image2bis.setVisibility(View.INVISIBLE);
                        }
                    }, 1500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique2.stop();
                        }
                    }, 2000);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique3.start();
                            musique3.setVolume(0.2f,0.2f);
                            Image3.setVisibility(View.VISIBLE);
                            Image3bis.setVisibility(View.INVISIBLE);
                        }
                    }, 2500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique3.stop();
                        }
                    }, 3000);



            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique4.start();
                            musique4.setVolume(0.2f,0.2f);
                            Image4.setVisibility(View.VISIBLE);
                            Image4bis.setVisibility(View.INVISIBLE);
                        }
                    }, 3500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique4.stop();

                        }
                    }, 4500);


        }
        if (60 <= tempsDoucheSecondes && tempsDoucheSecondes < 3 * 60) {
            TextView2.setText("Félicitations ! Tu parviens à prendre soin de toi mais aussi de la faune encore sauvage ! Ne change rien, tu es parfait, bisous");
            musique1 = MediaPlayer.create(this,R.raw.cling1);
            musique2 = MediaPlayer.create(this,R.raw.cling2);
            musique3 = MediaPlayer.create(this,R.raw.cling3);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique1.start();
                            musique1.setVolume(0.2f,0.2f);
                            Image1.setVisibility(View.VISIBLE);
                            Image1bis.setVisibility(View.INVISIBLE);
                        }
                    }, 500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique1.stop();

                        }
                    }, 1000);


            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique2.start();
                            musique2.setVolume(0.2f,0.2f);
                            Image2.setVisibility(View.VISIBLE);
                            Image2bis.setVisibility(View.INVISIBLE);
                        }
                    }, 1500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique2.stop();

                        }
                    }, 2000);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique3.start();
                            musique3.setVolume(0.2f,0.2f);
                            Image3.setVisibility(View.VISIBLE);
                            Image3bis.setVisibility(View.INVISIBLE);
                        }
                    }, 2500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique3.stop();

                        }
                    }, 3500);


        }
        if (tempsDoucheSecondes >= 3 * 60 && tempsDoucheSecondes < 5 * 60) {
            TextView2.setText("Temps raisonnable. Cependant, ne te reposes pas sur tes acquis et essayes d'améliorer encore ton efficacité pour sauver encore plus d'animaux en détresse. ");
            musique1 = MediaPlayer.create(this,R.raw.cling1);
            musique2 = MediaPlayer.create(this,R.raw.cling2);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique1.start();
                            musique1.setVolume(0.2f,0.2f);
                            Image1.setVisibility(View.VISIBLE);
                            Image1bis.setVisibility(View.INVISIBLE);
                        }
                    }, 500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique1.stop();

                        }
                    }, 1000);


            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique2.start();
                            musique2.setVolume(0.2f,0.2f);
                            Image2.setVisibility(View.VISIBLE);
                            Image2bis.setVisibility(View.INVISIBLE);
                        }
                    }, 1500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique2.stop();

                        }
                    }, 2500);



        }
        if (tempsDoucheSecondes >= 5 * 60 && tempsDoucheSecondes < 10 * 60 ) {
            TextView2.setText("Ta douche est vraiment trop longue ! Tu devrais avoir honte ! Il faut impérativement changer tes habitudes !");
            musique1 = MediaPlayer.create(this,R.raw.cling1);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique1.start();
                            musique1.setVolume(0.2f,0.2f);
                            Image1.setVisibility(View.VISIBLE);
                            Image1bis.setVisibility(View.INVISIBLE);
                        }
                    }, 500);

            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            musique1.stop();

                        }
                    }, 1500);

        }
        if (tempsDoucheSecondes >= 10 * 60   ) {
            TextView2.setText("Chenapan ! L'eau est une ressource rare, tu le regretteras dans 50 ans ! ");

        }
    }



    public void versConseils(View view) {
        if (tempsDoucheSecondes >= 5 * 60 && tempsDoucheSecondes < 10 * 60)  {
            musique1.stop();
        }
        if (tempsDoucheSecondes >= 3 * 60 && tempsDoucheSecondes < 5 * 60) {          //stop la musique si l'user n'a pas laissé le temps aux étoiles de faire leur bruit avant de cliquer ailleurs
            musique1.stop();
            musique2.stop();
        }
        if (60 <= tempsDoucheSecondes && tempsDoucheSecondes < 3 * 60) {            //les if evitent de stopper une musique qui n'a pas commencé
            musique1.stop();
            musique2.stop();
            musique3.stop();
        }
        if (tempsDoucheSecondes < 60) {
            musique1.stop();
            musique2.stop();
            musique3.stop();
            musique4.stop();
        }
        Intent versConseilsActivity = new Intent();
        versConseilsActivity.setClass(this, ConseilsActivity.class); //vers la classe ConseilsActivity
        startActivity(versConseilsActivity);
        finish();
        }

    public void versStatistiques(View view) {
        if (tempsDoucheSecondes >= 5 * 60 && tempsDoucheSecondes < 10 * 60)  {
            musique1.stop();
        }
        if (tempsDoucheSecondes >= 3 * 60 && tempsDoucheSecondes < 5 * 60) {
            musique1.stop();
            musique2.stop();
        }
        if (60 <= tempsDoucheSecondes && tempsDoucheSecondes < 3 * 60) {    //les if evitent de stopper une musique qui n'a pas commencé
            musique1.stop();
            musique2.stop();
            musique3.stop();
        }
        if (tempsDoucheSecondes < 60) {
            musique1.stop();
            musique2.stop();
            musique3.stop();
            musique4.stop();
        }
        Intent versStatistiquesActivity = new Intent();
        versStatistiquesActivity.setClass(this, StatistiquesActivity.class); //vers la classe StatistiquesActivity
        startActivity(versStatistiquesActivity);
        finish();
    }

    public void versMenu(View view) {
        if (tempsDoucheSecondes >= 5 * 60 && tempsDoucheSecondes < 10 * 60)  {
            musique1.stop();
        }
        if (tempsDoucheSecondes >= 3 * 60 && tempsDoucheSecondes < 5 * 60) {
            musique1.stop();
            musique2.stop();
        }
        if (60 <= tempsDoucheSecondes && tempsDoucheSecondes < 3 * 60) {   //les if evitent de stopper une musique qui n'a pas commencé
            musique1.stop();
            musique2.stop();
            musique3.stop();
        }
        if (tempsDoucheSecondes < 60) {
            musique1.stop();
            musique2.stop();
            musique3.stop();
            musique4.stop();
        }
        Intent versMenuActivity = new Intent();
        versMenuActivity.setClass(this, MenuActivity.class); //vers la classe MenuActivity
        startActivity(versMenuActivity);
        finish();
    }


}

